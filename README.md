# Install

Install Python 3.10.x

python3 -m pip install opencv-python
python3 -m pip install pillow

# How to use

- Connect your RTSP camera to your computer (same IP range).

- Update the RTSP url at the start of the script:

``rtsp_url = "rtsp://admin:123456789a@192.168.1.170"``

- Run the application:

``python3 002_main_no_buffer.py``

- Point the camera at the running timer on hte screen, and click "Capture Photo".

- The captured timestamp should show in the bottom image, along with the timestamp the image was taken. 

# Calculating Latency

The different between the time in the lower photo, and the frozen timestamp is the camera latency.

Example:

![example](example_calculation.png)


