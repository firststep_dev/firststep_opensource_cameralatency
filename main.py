import cv2
import tkinter as tk
from PIL import Image, ImageTk
import subprocess
import re
import datetime

# RTSP stream URL
#rtsp_url = "rtsp://admin:admin1234@192.168.4.200"
rtsp_url = "rtsp://admin:123456789a@192.168.1.170"

# ==============================================================================
# bufferless VideoCapture
#https://github.com/raymondlo84/nvidia-jetson-ai-monitor/blob/master/videocapturebufferless.py
import cv2
import queue
import threading
import time

class VideoCapture:
    def __init__(self, name):
        self.cap = cv2.VideoCapture(name)
        self.q = queue.Queue(maxsize=1)
        self.stop_capture = False
        self.lock = threading.Lock()
        self.discarded_frames = 0
        self.thread = threading.Thread(target=self._reader)
        self.thread.start()

    def _reader(self):
        while not self.stop_capture:
            ret, frame = self.cap.read()
            if not ret:
                self.q.put((False, False))
                break
            with self.lock:
                if not self.q.empty():
                    self.discarded_frames += 1
                    try:
                        self.q.get_nowait()  # discard previous (unprocessed) frame
                    except queue.Empty:
                        pass
                self.q.put((True, frame))

    def read(self):
        start_time = time.time()
        ret, frame = self.q.get()
        end_time = time.time()
        elapsed_time = end_time - start_time
        #print("Request time: %06.3f ms" % (1000*elapsed_time), ". Discarded frames:", self.discarded_frames)
        self.discarded_frames = 0  # Reset discarded frames count
        return ret, frame

    def release(self):
        self.stop_capture = True
        self.thread.join()
        self.cap.release()

    def isOpened(self):
        return self.cap.isOpened()

# ==============================================================================

def close_app():
    cap.release()
    exit()

# ==============================================================================

cap = VideoCapture(rtsp_url)

if (not cap.isOpened()):
    logging.error ("Camera connection: FAILED")
    logging.info ("Exiting main application...")
    exit()

# Set the width of the video frame
frame_width = 600

# Create a GUI window using Tkinter
window = tk.Tk()
window.title("RTSP Stream")
window.geometry("800x800")

# Create label 1 to display above the live video feed
label1 = tk.Label(window, text="Label 1", font=("Arial", 30))
label1.pack(side=tk.TOP)

# Create a label to display the live video feed
video_label = tk.Label(window)
video_label.pack(side=tk.TOP)

# Create label 2 to display above the captured photo
label2 = tk.Label(window, text="Label 2", font=("Arial", 30))
label2.pack(side=tk.TOP)

# Create a label to display the captured photo
photo_label = tk.Label(window)
photo_label.pack(side=tk.TOP)

# Function to convert the OpenCV frame to PIL image
def convert_frame_to_image(frame):
    # Convert the frame from BGR to RGB
    frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    # Create a PIL image from the RGB frame
    image = Image.fromarray(frame_rgb)

    return image

# Function to capture a photo from the live stream
def capture_photo():

    current_seconds = (datetime.datetime.now()+datetime.timedelta(milliseconds=4)).strftime("%S")
    while ((datetime.datetime.now()+datetime.timedelta(milliseconds=4)).strftime("%S") == current_seconds):
        pass

    # Read a frame from the video stream
    ret, frame = cap.read()

    if ret:

        # Resize the frame to the desired width
        frame = cv2.resize(frame, (frame_width, int(frame.shape[0] * frame_width / frame.shape[1])))

        # Convert the frame to a PIL image
        image = convert_frame_to_image(frame)

        # Convert the PIL image to PhotoImage
        photo_image = ImageTk.PhotoImage(image)

        # Display the captured photo
        photo_label.configure(image=photo_image)
        photo_label.image = photo_image

        current_time = datetime.datetime.now().strftime("%H:%M:%S.%f")[:-3]
        capture_time = f"Capture Time: {current_time}"
        label2.configure(text=capture_time)

# Create a button to capture a photo
capture_button = tk.Button(window, text="Capture Photo", command=capture_photo)
capture_button.pack(pady=10)

# Create a button to capture a photo
close_button = tk.Button(window, text="Close", command=close_app)
close_button.pack(pady=10)

# Function to update the video feed
def update_video_feed():
    # Read a frame from the video stream
    ret, frame = cap.read()

    if ret:
        # Resize the frame to the desired width
        frame = cv2.resize(frame, (frame_width, int(frame.shape[0] * frame_width / frame.shape[1])))

        # Convert the frame to a PIL image
        image = convert_frame_to_image(frame)

        # Convert the PIL image to PhotoImage
        video_image = ImageTk.PhotoImage(image)

        # Display the video feed
        video_label.configure(image=video_image)
        video_label.image = video_image

    current_time = datetime.datetime.now().strftime("%H:%M:%S.%f")[:-3]
    running_time = f"Current Time: {current_time}"
    label1.configure(text=running_time)

    # Schedule the next update
    window.after(10, update_video_feed)

# Start updating the video feed
update_video_feed()

# Start the Tkinter event loop
window.mainloop()

# Release the OpenCV capture object
cap.release()
